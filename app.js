const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const routes = require('./api-routes/routes');
const config = require('./config/config.json');
const port = process.env.PORTNUMBER || 8081;
global.businesslogic_path = config.businesslogic_path;

var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header(
        "Access-Control-Allow-Headers",
        "Access-Control-Allow-Headers, Origin, accessKey, X-Requested-With, Accept, Content-Type, "
    );
    res.header("Access-Control-Allow-Credentials", true);

    next();
};

app.use(allowCrossDomain);

app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
new routes(app);
app.listen(port);
console.info('server is running in port ', port);



app.get('/', (req, res) => {
    console.info('Dummy route to check server is working or not');
    res.send('Success');
});
