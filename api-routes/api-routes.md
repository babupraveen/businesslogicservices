# List of APIs

- [getSlotFlow](#getSlotFlow)
- [getBotFlow](#getBotFlow)
- [getValidationFunctionResult](#getValidationFunctionResult)
- [getSetFunctionResut](#getSetFunctionResut)

# getSlotFlow

`GET /api/bots/{botname}/intents/{intentname}/slotflow`

## Response

### Success: 200

JSON response

```
[{
    "name": "application",
    "action": "elicitSlot",
    "slotConstraint": "Required",
    "priority": 1,
    "request_message": "Sure! I can help you with that. But first let me know if it is the system login or any other application",
    "prompts": {
        "first": ["Sure, what is the name of the application you are facing the issue with"],
        "second": ["Application name should be something like Salesforce, Outlook etc. @newline Can I get the application name one more time?"],
        "third": ["An example could be Salesforce, Outlook etc. @newline Can you please give me the app name one last time"],
        "reason": ["Application name helps in giving you the right resolution. @newline Please give me the name of the application name"],
        "skip": ["Application name is mandatory to fix this issue @newline If you wish to talk to a human agent, type in 'Chat with agent'"],
        "needAttention": ["Looks like you need special attention to resolve this issue. If you wish to talk to a human agent please type in 'chat with agent'"]
    }
}, {
    "name": "firstname",
    "action": "elicitSlot",
    "slotConstraint": "Required",
    "priority": 2,
    "request_message": "Okay. Please tell me your full name",
    "prompts": {
        "first": ["Sure, may I get your name please?"],
        "second": ["Okay, Can you tell me your name again?"],
        "third": ["Please tell me your name one last time?"],
        "reason": ["Name is requied for our records"],
        "skip": ["Name is required to process your request @newline If you wish to talk to a human agent, type in 'Chat with agent'"],
        "needAttention": ["Looks like you need special attention to resolve this issue. If you wish to talk to a human agent please type in 'chat with agent'"]
    }
}, {
    "name": "employeeid",
    "action": "elicitSlot",
    "slotConstraint": "Required",
    "priority": 3,
    "request_message": "Can I get your employee id please?",
    "prompts": {
        "first": ["May I get your employee ID?"],
        "second": ["Employee id is a 4 digit number. @newline Can you please give me your employee id."],
        "third": ["Can you give me your employee id one last time. @newline It should be a 4 digit number"],
        "reason": ["Employee id is required to process your request"],
        "skip": ["Employee id is required to proceed further @newline If you wish to talk to a human agent, type in 'Chat with agent'"],
        "needAttention": ["Looks like you need special attention to resolve this issue. If you wish to talk to a human agent please type in 'chat with agent'"]
    }
}, {
    "name": "email",
    "action": "elicitSlot",
    "slotConstraint": "Required",
    "priority": 4,
    "request_message": "Please tell me your email id?",
    "prompts": {
        "first": ["May I get the email ID?"],
        "second": ["Email id should be something like rob@gmail.com. @newline Can I get your email id?"],
        "third": ["Can I get your email ID one last time. @newline It should be like rob@gmail.com"],
        "reason": ["Email id is required for our records"],
        "skip": ["Email id is required to proceed further. If you wish to talk to a human agent, type in 'Chat with agent'"],
        "needAttention": ["Looks like you need special attention to resolve this issue. If you wish to talk to a human agent please type in 'chat with agent'"]
    }
}]

```
### Error Handling

#### 404 - Failed Response

`{
    status_code :404,
    status:Failed
}`

#### 500 - Server error

`{
    status_code :500,
    status: server error
}`
#### 400 - Bad request

`{
    status_code :400,
    status: Bad request
}`
# getBotFlow

`GET /api/bots/{botname}/botflow`

## Response

### Success: 200

JSON response

```
[
    {
      "intentname": "LoginIssues",
      "confirmationflag":true,
      "clarificationmessage": ["So, you want to reset your password. Did I get you right?"],
      "triggermessage": ["I want to reset my password", "I want to reset my Salesforce password", "I want to reset my outlook password"],
      "confirmationmessage": ["Shall i go head with create a case?"],
      "fulfillmentmessage": {
        "resolved": "success message",
        "unresolved": "failure message",
        "maybe": "may be case",
        "NA": ""
      }
    },
    {
      "intentname": "Neo_Help",
      "confirmationflag":false,
      "confirmationmessage": "Shall i go head with create a case?",
      "clarificationmessage": ["So, are you looking for help"],
      "fulfillmentmessage": {
        "resolved": "",
        "unresolved": "failure message",
        "maybe": "may be case",
        "NA": ""
      }
    },
    {
      "intentname": "TalktoHuman",
      "confirmationflag":false,
      "confirmationmessage": "Shall i go head with create a case?",
      "clarificationmessage": ["So, you want to talk to our specialist?"],
      "fulfillmentmessage": {
        "resolved": "",
        "unresolved": "failure message",
        "maybe": "may be case",
        "NA": ""
      }
    }
]

```
### Error Handling

#### 404 - Failed Response

`{
    status_code :404,
    status:Failed
}`

#### 500 - Server error

`{
    status_code :500,
    status: server error
}`

#### 400 - Bad request

`{
    status_code :400,
    status: Bad request
}`

# validation method 

`GET /api/bots/{botname}/intents/{intentname}/validation/{val_function_name}/{input}`

## Response

### Success: 200

JSON response

```
{
          "validation_status": true/false,
          "validation_type": "",
          "name": "application" 
        }
```
### Error Handling

#### 404 - Failed Response

`{
    status_code :404,
    status:Failed
}`

#### 500 - Server error

`{
    status_code :500,
    status: server error
}`

#### 400 - Bad request

`{
    status_code :400,
    status: Bad request
}`

# set function 

`GET /api/bots/{botname}/intents/{intentname}/setfunc/{set_entityname_function_name}/{input}`

## Response

### Success: 200

response : string / object

message : ""


### Error Handling

#### 404 - Failed Response

`{
    status_code :404,
    status:Failed
}`

#### 500 - Server error

`{
    status_code :500,
    status: server error
}`

#### 400 - Bad request

`{
    status_code :400,
    status: Bad request
}`
