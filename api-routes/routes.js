/*
   Author : Praveen
   published : 12/14/2020
   purpose : To serve runtime requests

*/
const model = require('../model/intent');

function businessLogicService(app) {
    app.get('/api/bots/:botname/intents/:intentname/slotflow', async (req, res) => {
        try {
            if (req && req.params) {
                var modelLayer = new model(req.params.botname, req.params.intentname);
                var result = await modelLayer.getSlotFlowData();
                let finalresult = checkDataAndSendResultToClient(result);
                res.send(finalresult ? finalresult : {});
            } else {
                res.send(returnErrorCode);
            }
        } catch (error) {
            console.error('error from the slotflow api service ', error, error.stack);
        }
    });

    app.get('/api/bots/:botname/botflow', async (req, res) => {
        try {
            if (req && req.params) {
                var modelLayer = new model(req.params.botname);
                var result = await modelLayer.getBotFlowData();
                let finalresult = checkDataAndSendResultToClient(result);
                res.send(finalresult ? finalresult : {});
            } else {
                res.send(returnErrorCode);
            }
        } catch (error) {
            console.error('error from the botflow api service ', error, error.stack);
        }
    });

    app.get('/api/bots/:botname/intents/:intentname/validation/:functionname/:input', async (req, res) => {
        try {
            if (req && req.params) {
                var modelLayer = new model(req.params.botname, req.params.intentname, req.params.functionname, req.params.input);
                var result = await modelLayer.getValidationResult();
                let finalresult = checkDataAndSendResultToClient(result);
                res.send(finalresult ? finalresult : {});
            } else {
                res.send(returnErrorCode);
            }
        } catch (error) {
            console.error('Error from ');
        }
    });

    app.get('/api/bots/:botname/intents/:intentname/setfunc', async (req, res) => {
        try {
            if (req && req.params) {
                var modelLayer = new model(req.params.botname, req.params.intentname);
                var result = await modelLayer.getSetFuncResult();
                let finalresult = checkDataAndSendResultToClient(result);
                res.send(finalresult ? finalresult : {});
            } else {
                res.send(returnErrorCode);
            }
        } catch (error) {
            console.error('Error from ');
        }

    });

    app.get('/api/bots/:botname/intents/:intentname/setfunc/:functionname/:input', async (req, res) => {
        try {
            if (req && req.params) {
                // console.info('input is ', req.params.input, ' type is ', typeof req.params.input);
                var modelLayer = new model(req.params.botname, req.params.intentname, req.params.functionname, req.params.input);
                var result = await modelLayer.getfunctionData();
                let finalresult = checkDataAndSendResultToClient(result);
                res.send(finalresult ? finalresult : {});
            } else {
                res.send(returnErrorCode);
            }
        } catch (error) {
            console.error('Error from ', error, error.stack);
        }
    });
}

module.exports = businessLogicService;


const checkDataAndSendResultToClient = (data) => {
    try {
        if (data) {
            return {
                "status_code": 200,
                "status": "Success",
                "response": JSON.stringify(data)
            };
        } else {
            return {
                "status_code": 404,
                "status": "Failed"
            };
        }
    } catch (error) {
        console.error('Error from routes layer ', error, error.stack);
    }
};

const returnErrorCode = () => {
    return {
        "status_code": 500,
        "status": "Internal server error"
    };
};
