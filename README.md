# What is IntentConfigServices?
These are the set of backend microservices which helps in the runtime when user interacts with the bot after creating from platform.

# Folder structure
```
|-.DS_Store
|_README.md
|_api-routes
| |-api-routes.md
|-.gitignore
|-subscribers
|-app.js
```

Folder/File | Description
------------|------------
app.js | The main server file in nodejs that runs on a port number XXXX on the instance.
api-routes | application routes

# API Routes
Following are the API routes that are part of this service

API Name | Type | Input | Output | Definition
---------|------|-------|--------|-----------
getSlotFlow | GET | botname ,intentname | JSON map consits of all intent slots data | API called  when user interacts with bot in the runtime

getBotFlow | GET | botname,intentname |JSON map consits of all intent info like fulfillment and confirm states | API called  when user interacts with bot in the runtime

getValidationFunctionResult | GET | botname,intentname, slotname,function name | it will give success promise which consits of slot validation info

getSetFunctionResut | GET| botname,intent name,entityname,set function name| It will give response as a string / object 


# What is the Hack in Business Logic service?
The Hack is we are making a small change in the node module to pass one if condition which is in sync.js file, in line no. 15. The file path is- node_module/require_from_url/sync.js.

```
function sync(urlString) {
    var url = parse(urlString),
        href = url.href;
        if(true) {//!resolves[href]) {
        var returns = spawnSync('node', ['fetch.js', href], options);
        var err_str_filename = JSON.parse(returns.stdout.toString());
        if(err_str_filename[0]) throw new Error(err_str_filename[0]);
        resolves[href] = requireFromString(err_str_filename[1], err_str_filename[2]);
    }
    return resolves[href];
}
```



* It will not cause any time delay, the condition will check if the variable is true or false.
* This condition is handling the url related to the intent.js file, so it won’t cause any problem.
* There is a document for deployment, we have to mention this hack there in the business logic deployment section.
