The model constitutes of the following objects:
* Slot Flow JSON
* Bot Flow JSON
* Intent set & validation functions

## Slot Flow JSON
#### Attributes
Flow - JSON Object

#### Methods
getFlow - returns the flow JSON object
~setFlow~ - ~BL doesn't allow updating the slot flow file~

## Bot Flow JSON
#### Attributes
Flow - JSON Object

#### Methods
getFlow - returns the flow JSON object
~setFlow~ - ~BL doesn't allow updating the bot flow file~

## Intent set & validation functions
