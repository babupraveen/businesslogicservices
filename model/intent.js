/*
   Author : Praveen
   published : 12/14/2020
   purpose : get&set methods for all routes

*/

const services = require('../services/service');
const servicelayer = new services();
class intent {

    constructor(botname, intentname, functionname, input) {
        this.botname = botname;
        this.intentname = intentname;
        this.functionname = functionname;
        this.input = input;
        // console.debug('Input: '+input);
        // console.debug('Botname: '+botname);
        // console.debug('IntentName: '+intentname);
        // console.debug('functionname: '+functionname);
    }
    async getSlotFlowData() {
        let result = await servicelayer.getSlotsInfoFromEndPoint(this.botname, this.intentname);
        // console.info('SlotFlow Result is ', JSON.stringify(result));
        if (result)
            return result;
        else
            return null;
    }
    async getBotFlowData() {
        let result = await servicelayer.getBotInfoFromEndPoint(this.botname);
        // console.info('BotFlow Result is ', JSON.stringify(result));
        if (result && result.length > 0)
            return result;
        else
            return null;
    }

    async getValidationResult() {
        let result = await servicelayer.getSlotValidationResultFromEndPoint(this.botname, this.intentname, this.functionname, this.input);
        // console.info('Validation Result is ', JSON.stringify(result));
        if (result)
            return result;
        else
            return null;
    }

    async getSetFuncResult() {
        let result = await servicelayer.getSetFuncResultFromEndPoint(this.botname, this.intentname);
        console.debug('Response: ', JSON.stringify(result));
        if (result)
            return result;
        else
            return null;
    }

    async getfunctionData() {
        let result = await servicelayer.getSetFunctionData(this.botname, this.intentname, this.functionname, this.input);
        console.info('SetFunction Result is ', JSON.stringify(result));
        if (result)
            return result;
        else
            return null;
    }


}

module.exports = intent;
