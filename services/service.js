/*
   Author : Praveen
   published : 12/14/2020
   purpose : service layer for all routes

*/
const fetch = require('node-fetch');
const request = require('async-request');
const requireFromUrl = require('require-from-url/sync');
class service {

    async getSlotsInfoFromEndPoint(botname, intentname) {
        try {
            let slotflowfile;

            if (intentname && intentname !== 'Fallback') {
                let url = businesslogic_path + "/bot/" + botname + "/intents/" + intentname + "/slot_flow.json";
                let result = await checkFileData(url);
                console.info('result is ', result);
                if (!result) {
                    console.info('File path in if');
                    let bl_file = businesslogic_path + '/bot/' + botname + '/intents/' + intentname + '/' + intentname + '.js';
                    let intentsetactions = requireFromUrl(bl_file);
                    let result = await intentsetactions["set_fulfillment"]();
                    return result;
                } else {
                    if (botname && intentname) {
                        slotflowfile = await request(businesslogic_path + "/bot/" + botname + "/intents/" + intentname + "/slot_flow.json");
                        // console.info('slotflowfile in servicelayer ', JSON.stringify(slotflowfile));
                        if (slotflowfile && slotflowfile.statusCode != 404)
                            slotflowfile = JSON.parse(slotflowfile.body);
                        else
                            slotflowfile = null;
                        return slotflowfile;
                    } else {
                        return [];
                    }
                }
            }
        } catch (error) {
            console.error('Error from getSlotsInfoFromEndPoint function ', error, error.stack);
        }
    }


    async getBotInfoFromEndPoint(botname) {
        try {
            let botflowfile;
            if (botname) {
                botflowfile = await request(businesslogic_path + "/bot/" + botname + "/botflow.json");
                // console.info('slotflowfile in servicelayer ', JSON.stringify(botflowfile));
                if (botflowfile && botflowfile.statusCode != 404)
                    botflowfile = JSON.parse(botflowfile.body);
                else
                    botflowfile = null;
                return botflowfile;
            } else {
                return [];
            }
        } catch (error) {
            console.error('Error from getBotInfoFromEndPoint function ', error, error.stack);
        }
    }

    async getSlotValidationResultFromEndPoint(botname, intentname, funcname, input) {
        try {
            let result;
            let bl_file = businesslogic_path + '/bot/' + botname + '/intents/' + intentname + '/' + intentname + '.js';
            let intentsetactions = requireFromUrl(bl_file);
            if (intentsetactions && typeof intentsetactions[funcname] == 'function') {
                result = await intentsetactions[funcname](input);
                console.info('result from servicelayer is ', result);
            } else {
                result = null;
            }
            return result;
        } catch (error) {
            console.error('Error from getSlotValidationResult function ', error, error.stack);
        }
    }

    async getSetFuncResultFromEndPoint(botname, intentname) {
        try {
            let bl_file = businesslogic_path + '/bot/' + botname + '/intents/' + intentname + '/' + intentname + '.js';
            console.info('path is', bl_file);
            let intentsetactions = requireFromUrl(bl_file);
            console.info('Intent in service layer', intentsetactions);
            if (intentsetactions)
                return intentsetactions;
            else
                return null;
        } catch (error) {
            console.error('Error from getSetFuncResultFromEndPoint function ', error, error.stack);
        }
    }

    async getSetFunctionData(botname, intentname, functionname, input) {
        try {
            // console.info('Input is ', JSON.stringify(input), typeof input);
            let slots;
            let prompt;
            let message;
            if (input && typeof input == 'string' && !checkSpecialChars(input)) {
                input = input;
                prompt = input;
                console.info('prompt is ', prompt);
            } else if (typeof input == 'string' && checkSpecialChars(input)) {
                input = JSON.parse(input);
                slots = input.slots;
                prompt = input.prompt;
                console.info('prompt is ', prompt);
            }
            let bl_file = businesslogic_path + '/bot/' + botname + '/intents/' + intentname + '/' + intentname + '.js';
            let intentsetactions = requireFromUrl(bl_file);
            console.info('Intent in service layer', intentsetactions);
            if (intentsetactions && typeof intentsetactions[functionname] == 'function') {
                if (typeof input == 'object' && input.slots == undefined) {
                    message = await intentsetactions[functionname](input);
                } else if (typeof input == 'object' && input.slots !== undefined) {
                    // TODO: The following condition to swap the input is only to avoid changes across all the bots BL. Need to standardise
                    if(input.dialogState == 'Fulfilled' || functionname == 'set_fulfillment')
                      message = await intentsetactions[functionname](slots, prompt);
                    else
                      message = await intentsetactions[functionname](prompt, slots);
                } else {
                    message = await intentsetactions[functionname](input);
                }
                return message;
            } else {
                let slotflowfile = await request(businesslogic_path + "/bot/" + botname + "/intents/" + intentname + "/slot_flow.json");
                if (slotflowfile && slotflowfile.statusCode != 404) {
                    slotflowfile = JSON.parse(slotflowfile.body);
                    if (functionname && functionname.indexOf('set_') > -1) {
                        functionname = functionname.replace('set_', "");
                        // get Data from slotflow file
                        if (slotflowfile && slotflowfile.length > 0) {
                            for (var i = 0; i <= slotflowfile.length - 1; i++) {
                                if (slotflowfile[i].name == functionname) {
                                    let prompts = slotflowfile[i]['prompts'][prompt];
                                    console.info('prompts: ' + JSON.stringify(slotflowfile[i]['prompts']));
                                    return prompts[Math.floor(Math.random() * prompts.length)];
                                }
                            }
                        } else {
                            return null;
                        }
                    }
                } else {
                    return null;
                }
            }

        } catch (error) {
            console.error('Error from getSetFunctionData function ', error, error.stack);
        }
    }
}

function checkSpecialChars(str) {
    try {
        if (str && str.indexOf('{') == -1)
            return false;
        return true;
    } catch (error) {
        console.error('err is ', error);
    }
}

function checkFileData(url) {
    try {
        return new Promise((resolve, reject) => {
            fetch(url)
                .then(res => res.json())
                .then(json => {
                    // console.log('json is', json);
                    resolve(true);
                })
                .catch(err => {
                    console.error('error from fetch ', err, err.stack);
                    reject({});
                });
        }).catch(err => {
            console.error('error from promise ', err, err.stack);
            return false;
        });
    } catch (error) {
        console.error('Error from checkFileData ', error, error.stack);
        return false;
    }

}
module.exports = service;
